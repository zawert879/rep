class Chat {
    constructor(chatId = 0, fromId = 0) {
        this.chatId = chatId;
        this.fromId = fromId;
        this.status = 0;
        this.callback = () => { }
    }
}

class Chats {
    constructor() {
        /** @type {Array<Chat>}*/
        this.chats = [];

        setInterval(() => {
            this.chats = this.chats.filter(item => {
                return item.status !== 0
            })
        }, 180000);
    }

    firthOrCreate(chatId = 0, fromId = 0) {
        /**
         * @type {Chat}
         */
        let chat = this.chats.find(item => {
            if (item.chatId == chatId && item.fromId == fromId)
                return item;
        });
        if (chat === undefined) {
            chat = new Chat(chatId,fromId)
            this.chats.push(chat);
        }
        return chat;
    }
}

module.exports = new Chats();
module.exports.Chat = Chat;
//{ from: 252294750, chat: 252294750, status: 0, callback: ()=>{} }